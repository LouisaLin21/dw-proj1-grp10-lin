'use strict'
/**
Louisa Yuxin Lin and Qiuning Liang
 * to create an object project.
 */
function createProjectObject(projectId, ownerName, title, category, hours, rate, status, shortDescription) {
    let p = new Object();
    p.projectId = projectId;
    p.ownerName = ownerName;
    p.title = title;
    p.category = category;
    p.hours = hours;
    p.rate = rate;
    p.status = status;
    p.shortDescription = shortDescription;
    return p;
}

/**
 * Once the user fill out the form and it is correct, the current project is visually shown in the table and added to the adequate storage.
 * By Louisa and Qiuning
 */
function updateProjectsTable(project, insertFlag) {
    if(isEmpty(project)){
        return;
    }

    //shown in the table
    let tbody = document.getElementById('project-table-tbody');
    let tr = document.createElement('tr');
    let tdProJId = document.createElement('td');
    tdProJId.innerHTML = project.projectId;
    let tdOwnName = document.createElement('td');
    tdOwnName.innerHTML = project.ownerName;
    let tdTitle = document.createElement('td');
    tdTitle.innerHTML = project.title;
    let tdCategory = document.createElement('td');
    tdCategory.innerHTML = project.category;
    let tdHours = document.createElement('td');
    tdHours.innerHTML = project.hours;
    let tdRate = document.createElement('td');
    tdRate.innerHTML = project.rate;
    let tdStatus = document.createElement('td');
    tdStatus.innerHTML = project.status;
    let tdShortDescription = document.createElement('td');
    tdShortDescription.innerHTML = project.shortDescription;
    let tdEdit = document.createElement('td');
    tdEdit.innerHTML = editIcon;
    tdEdit.onclick = function editTableRow() {
        //save
        if (tdEdit.innerHTML.indexOf('fa-save') > 0) {
            let oldProjId = project.projectId;
            project.projectId = getValue(oldProjId + '-projId');
            project.ownerName = getValue(oldProjId + '-ownName');
            project.title = getValue(oldProjId + '-title');
            project.category = getValue(oldProjId + '-category');
            project.hours = getValue(oldProjId + '-hour');
            project.rate = getValue(oldProjId + '-rate');
            project.status = getValue(oldProjId + '-status');
            project.shortDescription = getValue(oldProjId + '-shortDesc');
            console.log("update all_projects_arr success. Latest content: \n" + JSON.stringify(all_projects_arr));

            tdProJId.innerHTML = getValue(oldProjId + '-projId');
            tdOwnName.innerHTML = getValue(oldProjId + '-ownName');
            tdTitle.innerHTML = getValue(oldProjId + '-title');
            tdCategory.innerHTML = getValue(oldProjId + '-category');
            tdHours.innerHTML = getValue(oldProjId + '-hour');
            tdRate.innerHTML = getValue(oldProjId + '-rate');
            tdStatus.innerHTML = getValue(oldProjId + '-status');
            tdShortDescription.innerHTML = getValue(oldProjId + '-shortDesc');
            tdEdit.innerHTML = editIcon;
        }
        //edit
        else {
            //When the icon (edit) next to the project is clicked, the whole rowbecome editable
            tdProJId.innerHTML = "<input id='" + project.projectId + "-projId' type='text'>";
            tdOwnName.innerHTML = "<input id='" + project.projectId + "-ownName' type='text' value='" + project.ownerName + "'>";
            tdTitle.innerHTML = "<input id='" + project.projectId + "-title' type='text' value='" + project.title + "'>";
            tdCategory.innerHTML = "<input id='" + project.projectId + "-category' type='text' value='" + project.category + "'>";
            tdHours.innerHTML = "<input id='" + project.projectId + "-hour' type='text' value='" + project.hours + "'>";
            tdRate.innerHTML = "<input id='" + project.projectId + "-rate' type='text' value='" + project.rate + "'>";
            tdStatus.innerHTML = "<input id='" + project.projectId + "-status' type='text' value='" + project.status + "'>";
            tdShortDescription.innerHTML = "<input id='" + project.projectId + "-shortDesc' type='text' value='" + project.shortDescription + "'>";
            tdEdit.innerHTML = saveIcon;
            //set focus
            let firstInput = document.getElementById(project.projectId + "-projId");
            firstInput.focus();
            firstInput.value = project.projectId;
        }
    }
    let tdDelete = document.createElement('td');
    tdDelete.innerHTML = deleteIcon;
    tdDelete.onclick = function deleteTableRow() {
        let id = project.projectId;
        let msg = "pls, confirm you want to delete project " + id;
        if (confirm(msg) == true) {
            //The project is then removed physically (storage) and logically (screen) 
            tr.remove();
            all_projects_arr.splice(all_projects_arr.findIndex(e => e.projectId === id), 1);
            console.log("delete all_projects_arr success. Latest content: \n" + JSON.stringify(all_projects_arr));
        }
    }
    tr.appendChild(tdProJId);
    tr.appendChild(tdOwnName);
    tr.appendChild(tdTitle);
    tr.appendChild(tdCategory);
    tr.appendChild(tdHours);
    tr.appendChild(tdRate);
    tr.appendChild(tdStatus);
    tr.appendChild(tdShortDescription);
    tr.appendChild(tdEdit);
    tr.appendChild(tdDelete);
    tbody.appendChild(tr);

    //added to the adequate storage
    if (insertFlag) {
        all_projects_arr.push(project);
        console.log("add all_projects_arr success. Latest content: \n" + JSON.stringify(all_projects_arr));
    }
}
/**
 * clear projects table  physically (storage) and logically (screen)
 * Qiuning
 */
function clearProjectsTable() {
    try {
        document.getElementById('project-table-tbody').innerHTML = '';
        all_projects_arr.splice(0, all_projects_arr.length);
        console.log("clear all_projects_arr success. Latest content: \n" + JSON.stringify(all_projects_arr));
    } catch (error) {
        console.log("An exception occurred while clearing: " + err.message);
        statusBarLog("An exception occurred while clearing!");
    }

}
/**
 * write-local button onclick event When the user click on button save to storage, all projects in the table are saved to the browser local storage.
 * Qiuning
 */
document.getElementById('write-local-button').onclick = function saveAllProjects2Storage() {
    if (all_projects_arr.length > 0) {
        let value = JSON.stringify(all_projects_arr);
        localStorage.setItem(projectsKey, value);
        console.log("write projectsDataBase success! latest content: \n" + localStorage.getItem(projectsKey));
        statusBarLog("Saving " + all_projects_arr.length + " projects in the local store!");
    } else {
        console.log("No data to write.");
        statusBarLog("Saving 0 projects in the local store!");
    }
}

/**
 * Same as with saveAllProjects2Storage, except that the new projects will be appended to a non-empty local storage
 *Qiuning
 */
document.getElementById('append-local-button').onclick = function appendAllProjects2Storage() {
    if (all_projects_arr.length > 0) {
        let projArr = JSON.parse(localStorage.getItem(projectsKey));
        if (isEmpty(projArr)) {
            statusBarLog('No existing local storage was detected, pls write(save) local storage first!');
        } else {
            let newAllArr = projArr.concat(all_projects_arr);
            let value = JSON.stringify(newAllArr);
            localStorage.setItem(projectsKey, value);
            console.log("append projectsDataBase success! latest content: \n" + localStorage.getItem(projectsKey));
            statusBarLog("Appending " + all_projects_arr.length + " projects in the local store!");
        }
    } else {
        statusBarLog("Appending 0 projects in the local store!");
    }
}
/**
 * When the user click on the button "clear local", all projects that are in local storage are removed.
 *Qiuning
 */
document.getElementById('clear-local-button').onclick = function clearAllProjectsFromStorage() {
    let projArr = JSON.parse(localStorage.getItem(projectsKey));
    if (isEmpty(projArr)) {
        statusBarLog("Local store is already cleared!");
    } else {
        statusBarLog("Clearing " + projArr.length + " projects in the local store!");
        localStorage.clear();
    }

}

/**
 * Read from the browser's local storage and visualize them to the user.
 * By Louisa
 */
document.getElementById('load-local-button').onclick = function readAllProjectsFromStorage() {
    let projArr = JSON.parse(localStorage.getItem(projectsKey));
    if (!isEmpty(projArr) && projArr.length > 0) {
        clearProjectsTable();
        projArr.forEach(project => {
            updateProjectsTable(project, true);
        });
        statusBarLog("Load " + projArr.length + " projects local store!");
    } else {
        statusBarLog("Load 0 projects local store!");
    }
}

/**
 * When the user enter any word(s) in the query input, those projects whose field values match the keyword are returned and rendered.
 * Qiuning Liang
 */
function filterProjects(queryMsg) {
    let counter = 0;
    if (all_projects_arr.length > 0) {
        document.getElementById('project-table-tbody').innerHTML = '';
        if (isEmpty(queryMsg)) {
            all_projects_arr.forEach(element => {
                updateProjectsTable(element, false);
                counter++;
            });
        } else {
            let query_arr = all_projects_arr.map(function (project) {
                if (project.projectId.indexOf(queryMsg) > -1 ||
                    project.ownerName.indexOf(queryMsg) > -1 ||
                    project.title.indexOf(queryMsg) > -1 ||
                    project.category.indexOf(queryMsg) > -1 ||
                    project.hours.indexOf(queryMsg) > -1 ||
                    project.rate.indexOf(queryMsg) > -1 ||
                    project.status.indexOf(queryMsg) > -1 ||
                    project.shortDescription.indexOf(queryMsg) > -1) {
                    console.log(1);
                    counter++;
                    return project;
                }
            });

            query_arr.forEach(element => {
                updateProjectsTable(element, false);
            });
        }
        console.log("all_projects_arr Latest content: \n" + JSON.stringify(all_projects_arr));
    }

    statusBarLog("You query returned " + counter + " results using keyword " + queryMsg + "!");
}
/**
 * Sort projects
 * By Louisa
 */
 function sortProjects(sortType) {
    if (all_projects_arr.length > 0) {
        document.getElementById('project-table-tbody').innerHTML = '';

        let query_arr;
        if(sortByProjId === sortType){
            query_arr = all_projects_arr.sort(function (project1,project2) {
                return project1.projectId.localeCompare(project2.projectId);
            });
        }else if(sortByOwnerName === sortType){
            query_arr = all_projects_arr.sort(function (project1,project2) {
                return project1.ownerName.localeCompare(project2.ownerName);
            });
        }

        query_arr.forEach(element => {
            updateProjectsTable(element, false);
        });
        console.log("all_projects_arr Latest content: \n" + JSON.stringify(all_projects_arr));
    }
}
/**
 * Enter event for query button
 * By Louisa
 */
document.getElementById('query-input').onkeydown = function queryInputOnEnterDown(e) {
    var theEvent = window.event || e;
    var code = theEvent.keyCode || theEvent.which || theEvent.charCode;
    if (code == 13) {
        filterProjects(getValue('query-input'));
    }
}

/**
 * Onblur event for query button
 * By Louisa
 */
document.getElementById('query-input').onblur = function queryInputOnblur() {
    filterProjects(getValue('query-input'));
}

/**
 * Onblur event for project id input
 * By Louisa
 */
document.getElementById('add-project-id').onblur = function () {
    if (!validateElement(document.getElementById('add-project-id').value, regexs.projectId)) {
        setElementFeedback('feedback-project-id', 'img-projId', 'Wrong format for proj id...', 'add-button');
    } else {
        validateSuccess('feedback-project-id', 'img-projId');
    }
}

/**
 * Onblur event for owner name input
 * By Louisa
 */
document.getElementById('add-owner-name').onblur = function () {
    if (!validateElement(document.getElementById('add-owner-name').value, regexs.ownerName)) {
        setElementFeedback('feedback-owner-name', 'img-owner-name', 'Wrong format for owner name...', 'add-button');
    } else {
        validateSuccess('feedback-owner-name', 'img-owner-name');
    }
}

/**
 * Onblur event for title input
 * By Louisa
 */
document.getElementById('add-title').onblur = function () {
    if (!validateElement(document.getElementById('add-title').value, regexs.title)) {
        setElementFeedback('feedback-title', 'img-title', 'Wrong format for title...', 'add-button');
    } else {
        validateSuccess('feedback-title', 'img-title');
    }
}

/**
 * Onblur event for category input
 * By Louisa
 */
document.getElementById('add-category').onblur = function () {
    if (!validateElement(document.getElementById('add-category').value, regexs.category)) {
        setElementFeedback('feedback-category', 'img-category', 'Wrong format for category...', 'add-button');
    } else {
        validateSuccess('feedback-category', 'img-category');
    }
}

/**
 * Onblur event for hours input
 * By Louisa
 */
document.getElementById('add-hours').onblur = function () {
    if (!validateElement(document.getElementById('add-hours').value, regexs.hours)) {
        setElementFeedback('feedback-hours', 'img-hours', 'Wrong format for hours...', 'add-button');
    } else {
        validateSuccess('feedback-hours', 'img-hours');
    }
}
/**
 * Onblur event for rate input
 * By Louisa
 */
document.getElementById('add-rate').onblur = function () {
    if (!validateElement(document.getElementById('add-rate').value, regexs.rate)) {
        setElementFeedback('feedback-rate', 'img-rate', 'Wrong format for rate...', 'add-button');
    } else {
        validateSuccess('feedback-rate', 'img-rate');
    }
}

/**
 * Onblur event for status input
 * By Louisa
 */
document.getElementById('add-status').onblur = function () {
    if (!validateElement(document.getElementById('add-status').value, regexs.status)) {
        setElementFeedback('feedback-status', 'img-status', 'Wrong format for status...', 'add-button');
    } else {
        validateSuccess('feedback-status', 'img-status');
    }
}

/**
 * Onclick event for add button
 * Qiuning Liang
 */
document.getElementById('add-button').onclick = function addNewProject() {
    if (isPass()) {
        let newProj = createProjectObject(
            getValue('add-project-id'),
            getValue('add-owner-name'),
            getValue('add-title'),
            getValue('add-category'),
            getValue('add-hours'),
            getValue('add-rate'),
            getValue('add-status'),
            getValue('add-short-descrption')
        )

        updateProjectsTable(newProj, true);

        statusBarLog('add new project success!');
    } else {
        document.getElementById('add-project-id').onblur();
    }
}

/**
 * Onclick event for reset button
 * Qiuning Liang
 */
document.getElementById('reset-button').onclick = function resetInputArea() {
    reset('add-project-id', 'img-projId', 'feedback-project-id');
    reset('add-owner-name', 'img-owner-name', 'feedback-owner-name');
    reset('add-title', 'img-title', 'feedback-title');
    resetSelect('add-category', 'img-category', 'feedback-category');
    reset('add-hours', 'img-hours', 'feedback-hours');
    reset('add-rate', 'img-rate', 'feedback-rate');
    resetSelect('add-status', 'img-status', 'feedback-status');
    reset('add-short-descrption');

    statusBarLog('reset success!');
}
/**
 * Onclick event for proj-id-sort button
 *By Louisa
 */
 document.getElementById('proj-id-sort').onclick = function sortByProId() {
    sortProjects(sortByProjId);
}

/**
 * Onclick event for owner-sort button
 *By Louisa
 */
 document.getElementById('owner-sort').onclick = function sortByOwner() {
    sortProjects(sortByOwnerName);
}
