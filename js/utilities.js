'use strict'
/**
 * A function that given an element id, it makes the element required and makes the font-weight of its label bold.
 * By Louisa
 */
function set_elem_required(elemId, labelId) {
    document.getElementById(elemId).required = true;
    document.getElementById(labelId).style.fontWeight = 'bold';
}

/**
 * A toggle function that changes the state of a button from disabled/enabled to enabled/disabled.
 * Qiuning Liang
 */
function enable_disable_Button(buttonId, disabled) {
    let button = document.getElementById(buttonId);
    if (disabled) {
        button.disabled = true;
        button.style.cursor = 'no-drop';
        button.style.backgroundColor = 'rgb(184, 184, 184)';
    } else {
        button.disabled = false;
        button.style.cursor = 'pointer';
        button.style.backgroundColor = 'rgba(91,209,215,1)';
    }
}

/**
 * Use the right pattern to check the element for which the event (blur) occur.
 * By Louisa
 */
function validateElement(str, regex) {
    return regex.test(str);
}

/**
 * Given elem_id to identify the field, a feedback_text, and error_success, add a feedback and the error image next to the element,
 * otherwise just the green valid image (without text)
 * Qiuning Liang
*/
function setElementFeedback(eleId, imgId, feedbackMsg, needDisabledButtonId) {
    document.getElementById(eleId).style.display = 'block';
    document.getElementById(eleId).innerHTML = "<p class='form-msg-p'>" + feedbackMsg + "</p>";
    document.getElementById(imgId).style.opacity = '100%';
    document.getElementById(imgId).children[0].src = wrongImg;
    enable_disable_Button(needDisabledButtonId, true);
}
/**
 * Called when the verification is successful
 * Qiuning Liang
 */
function validateSuccess(eleId, imgId) {
    document.getElementById(eleId).style.display = 'none';
    document.getElementById(imgId).style.opacity = '100%';
    document.getElementById(imgId).children[0].src = rightImg;

    if (isPass()) {
        enable_disable_Button('add-button', false);
    }
}
/**
 * Verify all elements of the form
 * Qiuning Liang
 */
function isPass() {
    if (document.getElementById('img-projId').children[0].src.endsWith(rightPng) &&
        document.getElementById('img-owner-name').children[0].src.endsWith(rightPng) &&
        document.getElementById('img-title').children[0].src.endsWith(rightPng) &&
        document.getElementById('img-category').children[0].src.endsWith(rightPng) &&
        document.getElementById('img-hours').children[0].src.endsWith(rightPng) &&
        document.getElementById('img-rate').children[0].src.endsWith(rightPng) &&
        document.getElementById('img-status').children[0].src.endsWith(rightPng)) {
        return true;
    } else {
        return false;
    }
}

/**
 * each time an operation is done, you should show a status message in the bar above the html table.
 * By Louisa
 */
function statusBarLog(msg) {
    document.getElementById('table-msg-p').innerHTML = msg;
}
/**
 * get element value
 * By Louisa
 */
function getValue(id) {
    return document.getElementById(id).value;
}

/**
 * Reset Form element
 * Qiuning Liang
 */
function reset(id, imgId, feedbackId) {
    document.getElementById(id).value = '';
    if (!isEmpty(imgId)) {
        document.getElementById(imgId).style.opacity = '0%';
    }
    if (!isEmpty(feedbackId)) {
        document.getElementById(feedbackId).style.display = 'none';
    }
}
/**
 * Reset Form element for selector
 * By Louisa
 */
function resetSelect(id, imgId, feedbackId) {
    document.getElementById(id).value = '---------';
    document.getElementById(imgId).style.opacity = '0%';
    document.getElementById(feedbackId).style.display = 'none';
}
/**
 * Check whether it is empty
 * By Louisa
 */
function isEmpty(str) {
    if (str == 'undefined' || !str || !/[^\s]/.test(str)) {
        return true;
    } else {
        return false;
    }
}
