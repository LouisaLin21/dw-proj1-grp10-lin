'use strict'
//a JS object literal to set your regex patterns
//Louisa Lin and Qiuning
const regexs = {
    projectId: /^[a-zA-Z\s\d]{2,}$/,
    ownerName: /^[a-zA-Z\s\d]{2,}$/,
    title: /^[a-zA-Z\s\d]{2,}$/,
    category: /^[a-z\s]*$/,
    hours: /^(\+?[1-9]\d{0,2}|\+?1000)$/,
    rate: /^([1-9]|[1-9]\\d|100)$/,
    status: /^[a-z\s]*$/,
    shortDescription: /^[a-zA-Z\s\d]{1,}$/,
}

//array to hold different project objects
const all_projects_arr = new Array();

const projectsKey = 'projectsDataBase';

//icon
const saveIcon = "<i class='fa fa-save cursor'></i>";
const editIcon = "<i class='fa fa-edit cursor'></i>";
const deleteIcon = "<i class='fa fa-trash cursor'></i>";

//png name
const wrongPng = "wrong.png";
const rightPng = "right.png";

//img src
const wrongImg = "../images/wrong.png";
const rightImg = "../images/right.png";

//sort type
const sortByProjId = 1;
const sortByOwnerName = 2;

